


import { GridyPagerImpl } from "../../gridy-grid/src/pager/impl/gridy-pager-impl.js";



export class GridyPagerDefault extends GridyPagerImpl {

    get pageBtnContainer() {
        if (! this._pageBtnContainer) {
            this._pageBtnContainer = this.comp.querySelector('.gridy-pager');
        }
        return this._pageBtnContainer;
    }

    set pageBtnContainer(pageBtnContainer) {
        this._pageBtnContainer = pageBtnContainer;
    }

    get subEls() {
        return [ 'pageBtnContainer' ];
    }

}